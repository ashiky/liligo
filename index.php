<?php
include("header.php");
?>
<main class="p-3">
    <h1 class="p-4 ps-0">Comparez tous les billets d'avion et trouvez un vol pas cher</h1>
    <h2 class="p-2 ps-0">Meilleures offres en ce moment</h2>
    <h3>Découvrez une sélection de destinations pour votre prochain voyage</h3>
    <div class="container d-flex flex-row">
        <div class="bg-dark w-50 m-2">
            <div class="card mx-auto">
                <img class="card-img mx-auto" src="/img/maria-teneva-KWu_pl00QEQ-unsplash.jpg" alt="">
                <div class="card-img-overlay">
                    <h5>Istanbul</h5>
                    <p class="text-white">26 janv, -2 février</p>
                    <h3 class="text-info">97 $</h3>
                </div>
            </div>
        </div>
        <div class="bg-dark w-50 m-2">
            <div class="card mx-auto">
                <img class=" car-img" src="/img/anton-lecock-CExAylTpX4w-unsplash.jpg" alt="">
                <div class="card-img-overlay mt-5">
                    <h5>Istanbul</h5>
                    <p class="text-white">26 janv, -2 février</p>
                    <h3 class="text-info">97 $</h3>
                </div>
            </div>
        </div>
    </div>
    <p>
        * Prix TTC constatés au cours des dernières 24 heures.
        Les prix des billets aller-retour sont susceptibles de varier.
        Les restrictions de voyage liées au COVID-19 peuvent évoluer,
        vérifiez les informations à jour avant votre départ.
    </p>
    <div class="container bg-primary-subtle mt-5">
        <img class="img-fluid" src="/img/home-page__conditions.webp" alt="">
        <h2>COVID-19 : vérifiez les conditions d’entrée</h2>
        <p>
            Avant de réserver votre voyage, vérifiez le détail des conditions d'entrée
            dans votre pays de destination et pour votre retour en France sur notre page dédiée.
        </p>
        <button type="button" class="btn btn-success w-100">Ou et comment voyager ?</button>
    </div>
    <div class="container bg-primary-subtle mt-5">
        <img class="img-fluid" src="/img/home-page__flexibility.webp" alt="">
        <h2>Réservez avec plus de flexibilité</h2>
        <p>
            Liligo vous aide à repérer les billets modifiables sans frais de dossier.
            Ils sont signalés par cette icône verte dans les résultats de votre recherche.
        </p>
        <button type="button" class="btn btn-success w-100">En savoir plus</button>
    </div>
    <h2>Comment trouver un vol pas cher ?</h2>
    <div class="container bg-primary ps-0 pe-0 pb-5 mb-5">
        <img class="img-fluid" src="/img/canmandawe-ftTsK4QinMw-unsplash.webp" alt="">
        <div class="p-2">
            <h3 class="text-white">1. Réservez votre billet à l'avance</h3>
            <p class="text-white">
                Réservez votre billet au minimum 8 mois à l’avance pour un vol long-courrier,
                5 à 6 mois à l’avance pour un vol moyen-courrier et 3 à 5 mois à l’avance pour un vol court-courrier.
            </p>
            <button type="button" class="btn btn-success w-100">En savoir plus</button>
        </div>
    </div>
    <h2>Le Magazine du voyageur</h2>
    <img class="img-fluid" src="/img/cadeaux-1200x700-1-716x716.webp" alt="">
    <h2 class="mt-3">Conseils et astuces pour voyager avec vos cadeaux</h2>
    <p>
        Vous avez trouvé un vol pas cher pour aller passer les fêtes loin de chez vous ?
        Vous êtes absolument ravi, mais soudain, l’inquiétude vous gagne.
        Comment allez-vous faire avec tous vos cadeaux dans l’avion ? Pas de panique,
        le petit Père Noël en vous peut dormir tranquille. Voici quelques conseils pour que tout arrive…
    </p>
    <img class="img-fluid" src="/img/istock-1072244930-716x716.webp" alt="">
    <h2>Les 10 plus belles cathédrales de France</h2>
    <div class="container">
        <div class="d-flex">
            <img class="img-fluid w-50 mb-3 pe-2" src="/img/koh-lipe-thailande-istock-358x267.webp" alt="">
            <h5 class="w-50 ps-2 mt-auto mb-auto">10 destinations de rêve où s’évader du quotidien</h5>
        </div>
        <div class="d-flex">
            <img class="img-fluid w-50 mb-3 pe-2" src="/img/londres-unsplash-358x267.webp" alt="">
            <h5 class="w-50 ps-2 mt-auto mb-auto">Les 10 plus belles villes anglaises à découvrir</h5>
        </div>
        <div class="d-flex">
            <img class="img-fluid w-50 mb-3 pe-2" src="/img/perito-moreno-glacier-argentine-1200x550-1-358x267.webp"
                alt="">
            <h5 class="w-50 ps-2 mt-auto mb-auto">Les 10 plus grands glaciers du monde</h5>
        </div>
        <div class="d-flex">
            <img class="img-fluid w-50 mb-3 pe-2" src="/img/perito-moreno-glacier-argentine-1200x550-1-358x267.webp"
                alt="">
            <h5 class="w-50 ps-2 mt-auto mb-auto">Où partir en janvier ?</h5>
        </div>
    </div>
    <div class="container d-flex flex-column">
        <h2>Ne manquez pas les bons plans voyage !</h2>
        <p>Inscrivez-vous à notre newsletter et recevez tous nos bons plans pour partir en toute sérénité.</p>
        <input class="mb-3" type="text" placeholder="Adresse e-mail">
        <input class="mb-3" type="text" placeholder="Ville de départ">
        <button type="button" class="btn btn-warning mb-3">s'inscrire</button>
        <img src="/img/liligo-passport.webp" alt="">
    </div>
    <h2 class="pt-3">Comment fonctionne le comparateur de vols Liligo ?</h2>
    <p>
        Liligo compare simultanément sur une même plateforme les offres de centaines de sites de voyages
        dans le monde pour vous permettre de trouver les meilleurs prix :
        vols, trains, bus, locations de voitures & hôtels. En utilisant un comparateur de vols comme Liligo,
        vous vous assurez de trouver vos billets d’avion au meilleur prix.
    </p>
    <img class="img-fluid" src="/img/home-page__how-1-deals.webp" alt="">
    <h3>Recherchez des offres de voyage</h3>
    <p>
        Effectuez une recherche sur notre comparateur de vols afin de trouver les offres disponibles pour votre
        destination.
        Indiquez votre ville de départ, votre ville de destination et vos dates de voyage,
        puis cliquez sur « Comparez » pour découvrir les meilleures offres de vol pas cher.
        Liligo compare aussi les voitures de location et les hôtels.
    </p>
    <img class="img-fluid border-top" src="/img/home-page__how-2-compare.webp" alt="">
    <h3>Comparez en toute transparence</h3>
    <p>Effectuez une recherche sur notre comparateur de vols afin de trouver les offres disponibles pour votre
        destination.
        Indiquez votre ville de départ, votre ville de destination et vos dates de voyage,
        puis cliquez sur « Comparez » pour découvrir les meilleures offres de vol pas cher.
        Liligo compare aussi les voitures de location et les hôtels.
    </p>
    <img class="img-fluid border-top" src="/img/home-page__how-3-booking.webp" alt="">
    <h3>Réservez auprès du partenaire</h3>
    <p>
        Liligo n’est pas un site marchand, mais uniquement un comparateur d’offres de voyages
        (billets d’avion, voitures de location et hôtels). Une fois l’offre sélectionnée,
        vous quittez le site de Liligo et êtes redirigés sur le site du partenaire pour effectuer votre réservation.
    </p>
    <img class="img-fluid border-top" src="/img/home-page__how-4-tools.webp" alt="">
    <h3>Utilisez nos outils de voyage gratuitement</h3>
    <p>
        Magazine du voyageur, newsletter, alertes de prix…
        trouvez l’inspiration dont vous avez besoin pour choisir votre destination,
        suivez l’évolution des prix des billets d’avion et restez informés des meilleures offres de voyage en temps réel
        !
    </p>
</main>























<script src="/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"></script>
</body>

</html>