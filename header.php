<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD">
    <script src="https://kit.fontawesome.com/08b0c186e9.js" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>
    <header class="bg-primary p-3">
        <nav class="navbar navbar-light bg-light">
            <div class="container">
                <div class="d-flex flex-column">
                    <a class="navbar-brand" href="#">
                        <i class="fa-solid fa-plane"></i>
                        <p>Vols</p>
                    </a>
                </div>
                <div class="d-flex flex-column">
                    <a class="navbar-brand" href="#">
                        <i class="fa-solid fa-car"></i>
                        <p>Voitures</p>
                    </a>
                </div>
                <div class="d-flex flex-column">
                    <a class="navbar-brand" href="#">
                        <i class="fa-solid fa-bed"></i>
                        <p>Hôtels</p>
                    </a>
                </div>
            </div>
        </nav>
        <div class="container d-flex flex-column bg-light mt-3 p-3">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Aller-retour
            </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Aller-retour</a></li>
                <li><a class="dropdown-item" href="#">Allersimple</a></li>
            </ul>
            <input class="m-2 p-2 bg-body-secondary" type="text" placeholder="Paris, France">
            <input class="m-2 p-2 bg-body-secondary" type="text" placeholder="Vers ?">
            <div class="d-flex flex-row m-auto">
                <input class="m-2 p-2 bg-body-secondary w-50" type="text" placeholder="mar.10 janv">
                <input class="m-2 p-2 bg-body-secondary w-50" type="text" placeholder="mar.17 janv">
            </div>
            <input class="m-2 p-2 bg-body-secondary" type="text" placeholder="1 Passager, Economique">
            <button type="button" class="btn btn-warning">COMPAREZ</button>
        </div>
    </header>
